#include <iostream>

#include "SRTX/Periodic_task.hpp"
#include "SRTX/Scheduler.hpp"

using namespace std;

class Noop : public SRTX::Periodic_task
{
    bool execute() override
    {
        return true;
    }
};

int main()
{
    const auto period = 10ms;
    SRTX::Scheduler<> s(period);

    auto t = make_shared<Noop>();
    s.schedule(t, period);
    s.start();

    cout << "*** PASSED! ***\n";
}
